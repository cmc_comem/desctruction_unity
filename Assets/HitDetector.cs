﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDetector : MonoBehaviour {

	public SimplifySocket ss;
	public int id;
	public int points = 10;

	public void Initiate(SimplifySocket ss, int id) {
		this.ss = ss;
		this.id = id;
		gameObject.SetActive (false);
	}

	public void Hit() {
		if (ss.targetMode == SimplifySocket.TargetMode.OneHit) {
			TargetManager.instance.SwitchTarget (id);
			gameObject.SetActive (false);
		}
	}

}
