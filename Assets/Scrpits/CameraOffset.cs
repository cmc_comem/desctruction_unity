﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraOffset : MonoBehaviour {
	public Vector3 offset = new Vector3(0,2,0);
	private Camera camera;
	public Matrix4x4 m;
	// Use this for initialization
	void Start () {
		camera = GetComponent<Camera> ();
		camera.projectionMatrix = m;
	}

	void Update() {
		camera.projectionMatrix = m;
	}
}
