﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {



	public void Explode(Vector3 position, float size) {
		Debug.Log (position + " _ " + size);
		Rigidbody[] rbs = GetComponentsInChildren<Rigidbody> ();
		transform.GetChild (0).localScale = new Vector3 (size / 10, size / 10, size / 10);
		foreach (var rb in rbs) {
			rb.AddExplosionForce(100 * size, position, 1 * size);
		}
		Debug.DrawRay (Camera.main.transform.position, position, Color.red, 2f);
		Invoke ("Deact", 0.4f * size);
	}

	void Deact() {
		gameObject.SetActive (false);
	}
}
