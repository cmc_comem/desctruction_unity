﻿using UnityEngine;
using System.Collections;

public class CellListner : MonoBehaviour {

	private Rigidbody rb;
	private Destruction parentDestruction;
	private float maxSize;

	void Start () {
		rb = GetComponent<Rigidbody> ();
		rb.isKinematic = true;
		parentDestruction = transform.parent.parent.GetComponent<Destruction> ();
		maxSize = Mathf.Max (transform.lossyScale.y, transform.lossyScale.x, transform.lossyScale.z);
	}

	void OnCollisionEnter(Collision coll) {
		if (coll.gameObject.name == "OutOfBound") {
			Destroy (gameObject);
		} else {
			if (coll.relativeVelocity.magnitude > parentDestruction.hitStrength && rb.isKinematic) {
				rb.isKinematic = false;
				parentDestruction.CellDestroyed ();
			}
		}
	}

//	void OnCollisionExit(Collision coll) {
//		RaycastHit hit;
//		if (!Physics.SphereCast(transform.position, maxSize, transform.forward, out hit, maxSize)) {
//			Fall ();
//		}
//	}

	public void Fall() {
		rb.isKinematic = false;
	}
}
