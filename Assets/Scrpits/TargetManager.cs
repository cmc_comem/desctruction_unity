﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour {

	public HitDetector[] targets;
	public SimplifySocket ss;

	public int simultaniousTargets = 3;

	public static TargetManager instance;

	// Use this for initialization
	void Start () {
		instance = this;
		if (ss == null) {
			ss = GameObject.Find ("GameManager").GetComponent<SimplifySocket> ();
		}
			
		targets = GetComponentsInChildren<HitDetector> ();

		for (int i = 0; i < targets.Length; i++) {
			targets [i].Initiate (ss, i);
		}	

		for (int i = 0; i < simultaniousTargets; i++) {
			SwitchTarget (-1);
		}

	}

	public void SwitchTarget(int oldTargetId) {
		bool foundOne = false;
		do {
			int id = Random.Range(0, targets.Length);
			if(!targets[id].gameObject.activeSelf && id != oldTargetId) {
				targets[id].gameObject.SetActive(true);
				foundOne = true;
			}
		} while (foundOne == false);
	}
}
