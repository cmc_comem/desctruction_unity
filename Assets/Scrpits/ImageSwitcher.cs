﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ImageSwitcher : MonoBehaviour {

	private static ImageSwitcher _instance;

	private int iteration;
	private SpriteRenderer sr;
	public Sprite[] sprites;

	void Awake () {
		if (!_instance) {
			_instance = this;
		} else {
			Destroy (gameObject);
		}
		sr = gameObject.GetComponent<SpriteRenderer> ();
	}

	void OnEnable () {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable () {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		Debug.Log (iteration);
		iteration = PlayerPrefs.GetInt ("imageNumber", 0);
		sr.sprite = sprites [iteration];

		iteration = (iteration + 1) % sprites.Length;
		PlayerPrefs.SetInt ("imageNumber", iteration);
	}
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
