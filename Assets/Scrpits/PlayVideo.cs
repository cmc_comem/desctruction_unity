﻿using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		MovieTexture mt = (MovieTexture)GetComponent<Renderer> ().material.mainTexture;
		mt.Play();
		mt.loop = true;
	}
}
