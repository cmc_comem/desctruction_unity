﻿using UnityEngine;
using System.Collections;

public class ClickListener : MonoBehaviour {

	public Shooter shooter;

	// Use this for initialization
	void Start () {
		if (shooter == null) {
			shooter = GameObject.Find("Shooter").GetComponent<Shooter> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)) {
			RaycastHit hitInfo;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hitInfo, 60)) {
				shooter.Shoot ("socketIDDDDDD", new Vector3 (hitInfo.point.x, hitInfo.point.y, -49), Color.green);
			}
		}
	}
}
