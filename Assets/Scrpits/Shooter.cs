﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

	public ObjectPooler bullets;
	public ObjectPooler explosions;
	private float bulletVelocity;
	public float swipeDivider = 300;

	public float errorDivider = 2;
	// Use this for initialization
	void Start () {
		if (bullets == null) {
			bullets = GameObject.Find ("Pooler").GetComponent<ObjectPooler> ();
		}
	}


	public void Shoot(string sockId, Vector3 target, Color color) {
		Vector3 velocity = HitTargetAtTime (transform.position, target, 0.5f);

		GameObject bullet = bullets.GetPooledObject ();
		bullet.transform.position = transform.position;
		bullet.SetActive (true);
		bullet.GetComponent<Bullet> ().Init (sockId);

		bullet.GetComponent<Renderer> ().material.color = color;
		Debug.DrawRay (Camera.main.transform.position, target, Color.red, 2f);
		Rigidbody rb = bullet.GetComponent<Rigidbody> ();
		rb.velocity = velocity;
	}

	public void Tap(string sockId, Vector3 target, float size) {
		GameObject explosion = explosions.GetPooledObject ();
		explosion.transform.position = target;
		explosion.SetActive (true);
		explosion.GetComponent<Explosion> ().Explode (target, size);
	}

	public void Shoot(string sockId, Vector3 direction, float velocity, Color color) {

		direction.x = direction.x / errorDivider;

		Vector3 velocity3 = direction.normalized * velocity/swipeDivider;

		GameObject bullet = bullets.GetPooledObject ();
		bullet.transform.position = transform.position;
		bullet.SetActive (true);
		bullet.GetComponent<Bullet> ().Init (sockId);

		bullet.GetComponent<Renderer> ().material.color = color;

		Rigidbody rb = bullet.GetComponent<Rigidbody> ();
		rb.velocity = velocity3;
	}

	public static Vector3 HitTargetAtTime(Vector3 startPosition, Vector3 targetPosition, float timeToTarget) {
		Vector3 gravityBase = Physics.gravity;
		Vector3 AtoB = targetPosition - startPosition;
		Vector3 horizontal = GetHorizontalVector(AtoB, gravityBase);
		float horizontalDistance = horizontal.magnitude;
		Vector3 vertical = GetVerticalVector(AtoB, gravityBase);
		float verticalDistance = vertical.magnitude * Mathf.Sign(Vector3.Dot(vertical, -gravityBase));

		float horizontalSpeed = horizontalDistance / timeToTarget;
		float verticalSpeed = (verticalDistance + ((0.5f * gravityBase.magnitude) * (timeToTarget * timeToTarget))) / timeToTarget;

		Vector3 launch = (horizontal.normalized * horizontalSpeed) - (gravityBase.normalized * verticalSpeed);
		return launch;
	}

	public static Vector3 GetHorizontalVector(Vector3 AtoB, Vector3 gravityBase) {
		Vector3 output;
		Vector3 perpendicular = Vector3.Cross(AtoB, gravityBase);
		perpendicular = Vector3.Cross(gravityBase, perpendicular);
		output = Vector3.Project(AtoB, perpendicular);
		return output;
	}

	public static Vector3 GetVerticalVector(Vector3 AtoB, Vector3 gravityBase) {
		Vector3 output;
		output = Vector3.Project(AtoB, gravityBase);
		return output;
	}
}
