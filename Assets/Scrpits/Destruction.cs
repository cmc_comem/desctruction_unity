﻿using UnityEngine;
using System.Collections;

public class Destruction : MonoBehaviour {

	public GameObject newObject;
	public GameObject brokenObject;
	public float destructionState;
	public float hitStrength = 3;
	public float collapseValue = 0.6f;

	private CellListner[] cells;
	private int totalCount;
	private int currentCount;

	// Use this for initialization
	void Start () {
		brokenObject.SetActive (false);
		newObject.SetActive (true);
		destructionState = 0;
	}
	
	public void Hit() {
		brokenObject.SetActive (true);
		newObject.SetActive (false);
		cells = brokenObject.GetComponentsInChildren<CellListner> ();
		totalCount = currentCount = cells.Length;
	}

	public void CellDestroyed() {
		currentCount -= 1;
		destructionState = (float)currentCount / totalCount;
		if (destructionState < collapseValue) {
			foreach (var cell in cells) {
				cell.Fall ();
			}
		}
	}
}
