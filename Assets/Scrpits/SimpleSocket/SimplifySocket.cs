﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using SocketIO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SimplifySocket : MonoBehaviour {

	/**********************************************
	 *              	Attributes 	              *
	 **********************************************/
	public enum TargetMode { OneHit, Timebase }

	// Public
	public Shooter shooter;
	public bool connectToLocal = false;
	public SocketIOComponent sock;
	public Dictionary<string, GameObject> players;
	public int maxConnectedPlayers = 0;
	public static SimplifySocket instance;
	public string gameType = "destruction";
	public TargetMode targetMode = TargetMode.OneHit;

	//Private	
	private Dictionary<string, ClientSocketHandler> socketHandlers;
	private Transform spawnPoint;


	/**********************************************
	 *              Public methods                *
	 **********************************************/

	public void RemovePlayer(string sockId){
		GameObject playerGameObject = ((MonoBehaviour)socketHandlers[sockId]).gameObject;
		socketHandlers.Remove(sockId);
		Destroy (playerGameObject);
	}

	/**********************************************
	 *                 Lifecycle                  *
	 **********************************************/


	public void Awake(){
		sock = GetComponent<SocketIOComponent> ();

		sock.url = "wss://ns3056388.ip-193-70-6.eu/socket.io/?EIO=4&transport=websocket"; // OVH: 149.202.223.139  DO: 46.101.152.216 OVH: ns3056388.ip-193-70-6.eu

		//todo - temporary remove
		//#if UNITY_EDITOR
		if (connectToLocal) {
			sock.url = "ws://localhost:3010/socket.io/?EIO=4&transport=websocket";
		} 
		//#endif
		sock.autoConnect = true;

	}

	void Start () {
		instance = this;
		socketHandlers = new Dictionary<string, ClientSocketHandler> ();
		if (shooter == null) {
			shooter = GameObject.Find ("Shooter").GetComponent<Shooter> ();
		}

		players = new Dictionary<string, GameObject> ();
		sock.On ("open", OnSocketOpen);
		sock.On ("msg", OnGotMessageFromClient);
		//sock.On ("reconnect", OnNodeServerDisconnect);
		// on disconnect
		sock.Connect ();
	}

	private void OnGotMessageFromClient(SocketIOEvent ev){
		string sockId = ev.data ["senderId"].str;
		string shotType = ev.data ["shotType"].str;
		Debug.Log (sockId);
		Color color;
		switch (shotType) {
		case "target":
			color = new Color (ev.data ["color"] ["r"].n, ev.data ["color"] ["g"].n, ev.data ["color"] ["b"].n);
			Vector3 position = new Vector3 (ev.data ["position"] ["x"].n, ev.data ["position"] ["y"].n, ev.data ["position"] ["z"].n);
			shooter.Shoot (sockId, position, color);
			break;
		case "swipe":
			color = new Color (ev.data ["color"] ["r"].n, ev.data ["color"] ["g"].n, ev.data ["color"] ["b"].n);
			Vector3 direction = new Vector3 (ev.data ["direction"] ["x"].n, ev.data ["direction"] ["y"].n, ev.data ["direction"] ["z"].n);
			float velocity = ev.data ["velocity"].f;
			shooter.Shoot (sockId, direction, velocity, color);
			break;
		case "tap":
			Vector3 target = new Vector3 (ev.data ["position"] ["x"].n, ev.data ["position"] ["y"].n, ev.data ["position"] ["z"].n);
			float size = ev.data ["size"].n;
			shooter.Tap (sockId, target, size);
			break;
		default:
			break;
		}

	}

	public void OnSocketOpen(SocketIOEvent ev){
		Debug.Log("updated socket id " + sock.sid);
		// Double connect for some reason
		if (sock.sid.Length > 0) {
			Identify ();
		}
	}

	private void Identify(){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["type"] = "host";
		data ["id"] = sock.sid;
		data ["gameType"] = gameType;
		sock.Emit("identify", new JSONObject(data));
	}

	void HandleSocketDisconnect(string sockId){
		// Simple for now, maybe create a more advanced system later
		if (sockId != null && socketHandlers.ContainsKey(sockId)) {
			socketHandlers.Remove(sockId);
		}
		if (players.ContainsKey (sockId)) {
			players.Remove (sockId);
		}
	}

	public void SendDataToController(string socketId, JSONObject data){

		if (socketId.Length > 0) {
			//Debug.Log ("Sending datas");
			data.AddField ("destinationId", socketId);
			sock.Emit("msg", data);
		} else {
			Debug.Log ("no socket id");
		}
		//data["destinationId"] = socketId;

		//Debug.Log ("Emitted data");
		//Debug.Log (data);
	}
		
	void Broadcast(JSONObject data){
		// Sends message to all the controllers
		data.AddField ("destinationId", "broadcast");
		sock.Emit("msg", data);
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.B)) {
			Dictionary<string, string> d = new Dictionary<string, string>();
			d["test"] = "test";
			Broadcast (new JSONObject(d));
			Debug.Log ("broadcast");
		} 
	}

}