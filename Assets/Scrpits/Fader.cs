﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour {

	private static Fader _instance;

	public float duration = 1.0f;

	float lerp;
	Color transparent = new Color (0, 0, 0, 0);
	Color black = new Color (0, 0, 0, 1);
	public Color initialColor;
	public Color finalColor;
	public Color lerpColor;
	Image img;

	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	void Awake() {
		if (!_instance) {
			_instance = this;
		} else {
			Destroy (gameObject);
		}
	}

	void Start() {
		DontDestroyOnLoad (gameObject);
		transform.GetChild (0).gameObject.SetActive (true);
		img = GetComponentInChildren<Image> ();
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		lerp = 0;
		initialColor = black;
		finalColor = transparent;
		GetComponent<Canvas> ().worldCamera = Camera.main;
	}

	void Update() {
		lerp += Time.deltaTime / duration; 
		lerpColor = Color.Lerp (initialColor, finalColor, lerp);
		img.color = lerpColor;
	}

	public void FadeOut() {
		lerp = 0;
		initialColor = transparent;
		finalColor = black;
	}
}
