﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bullet : MonoBehaviour {

	private string sockId;

	public void Init(string socket){
		sockId = socket;
		Invoke ("Deact", 50f);
		GetComponent<SphereCollider> ().enabled = false;
		Invoke ("ActivateColl", 0.3f);
	}

	void ActivateColl() {
		GetComponent<SphereCollider> ().enabled = true;
	}

	void Deact() {
		sockId = "";
		gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider c) {
		if (c.gameObject.tag == "Bucket") {
			int points = c.gameObject.GetComponent<HitDetector> ().points;
			c.gameObject.GetComponent<HitDetector> ().Hit ();
			Dictionary<string, string> data = new Dictionary<string, string> ();
			data ["points"] = points.ToString ();
			data ["msgType"] = "point";

			SimplifySocket.instance.SendDataToController (sockId, new JSONObject (data));
			Deact ();
			CancelInvoke ("Deact");
		} else if(c.gameObject.tag == "OutOfBound"){
			Deact ();
			CancelInvoke ("Deact");
		}
	}

	void OnCollisionEnter(Collision c) {
		if (c.gameObject.tag == "Target") {
			int points = c.gameObject.GetComponent<HitDetector> ().points;
			c.gameObject.GetComponent<HitDetector> ().Hit ();
			Dictionary<string, string> data = new Dictionary<string, string> ();
			data ["points"] = points.ToString ();
			data ["msgType"] = "point";

			SimplifySocket.instance.SendDataToController (sockId, new JSONObject (data));
		} else if (c.gameObject.tag == "OutOfBound") {
			CancelInvoke ("Deact");
			Deact ();
		} else if (c.gameObject.tag == "Cells") {
			if (c.gameObject.GetComponent<Rigidbody> ().isKinematic) {
				Debug.Log ("small");
				Dictionary<string, string> data = new Dictionary<string, string> ();
				data ["points"] = "1";
				data ["msgType"] = "point";

				Debug.Log (SimplifySocket.instance);
				SimplifySocket.instance.SendDataToController (sockId, new JSONObject (data));
				Debug.Log ("sent");
			}
		} else if (c.gameObject.tag == "Unbroken") {
			Debug.Log ("large");
			Dictionary<string, string> data = new Dictionary<string, string> ();
			data ["points"] = "3";
			data ["msgType"] = "point";

			SimplifySocket.instance.SendDataToController (sockId, new JSONObject (data));
		}
	}
}
