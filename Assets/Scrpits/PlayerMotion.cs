﻿using UnityEngine;
using System.Collections;

public class PlayerMotion : MonoBehaviour {

	public int shootingSpeed = 100;
	public float speed = 0.1f;
	public float mouseSpeed = 1f;
	public ObjectPooler bullets;
	public Camera cam;
	private bool isShooting = false;

	// Use this for initialization
	void Start () {
		if (bullets == null) {
			bullets = GameObject.Find ("Pooler").GetComponent<ObjectPooler> ();
		}
		if (cam == null) {
			cam = Camera.main;
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = transform.position;

		if (Input.GetAxis ("Horizontal") > 0) {
			pos = new Vector3(pos.x - speed * Input.GetAxis ("Horizontal"), pos.y, pos.z);
		} else if(Input.GetAxis("Horizontal") < 0) {
			pos = new Vector3 (pos.x - speed * Input.GetAxis ("Horizontal"), pos.y, pos.z);
		}

		if (Input.GetAxis ("Vertical") > 0) {
			pos = new Vector3(pos.x, pos.y, pos.z - speed * Input.GetAxis ("Vertical"));
		} else if(Input.GetAxis("Vertical") < 0) {
			pos = new Vector3 (pos.x, pos.y, pos.z - speed * Input.GetAxis ("Vertical"));
		}

		transform.position = pos;

		transform.Rotate(new Vector3(0f , Input.GetAxis("Mouse X"), 0f) * Time.deltaTime * mouseSpeed);
		cam.transform.Rotate (new Vector3 (-Input.GetAxis ("Mouse Y"), 0f, 0f) * Time.deltaTime * mouseSpeed);

		if (Input.GetKey (KeyCode.Space) && !isShooting) {
			isShooting = true;
			Shoot ();
		}
	}

	void Shoot() {
		isShooting = true;
		GameObject bullet = bullets.GetPooledObject ();
		bullet.transform.position = transform.position;
		bullet.SetActive (true);
		Rigidbody rb = bullet.GetComponent<Rigidbody> ();
		rb.AddForce (cam.transform.forward * shootingSpeed, ForceMode.Force);
		bullet.GetComponent<Bullet> ().Init(null);
		Invoke ("DoneShooting", 0.1f);
	}

	void DoneShooting() {
		isShooting = false;
	}
}
