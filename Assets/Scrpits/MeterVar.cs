﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MeterVar : MonoBehaviour {

	public Fader fader;

	public float facadeStrength = 100f;
	public float cellStrength = 100f;

	public DestructionMeter[] sections;
	public float sectionDestroyed;
	public float sectionDestroyedPercentage;
	public float endPercentage = 0.6f;

	public static float _facadeStrength;
	public static float _cellStrength;

	private bool isDestroyed = false;

	void Awake() {
		_facadeStrength = facadeStrength;
		_cellStrength = cellStrength;
	}

	void Start() {
		sections = GetComponentsInChildren<DestructionMeter> ();
		if (fader == null) {
			fader = GameObject.Find ("Fader").GetComponentInChildren<Fader> ();
		}
	}

	public void SectionDestoryed() {
		sectionDestroyed += 1;
		sectionDestroyedPercentage = sectionDestroyed / sections.Length;
		if (sectionDestroyedPercentage > endPercentage && !isDestroyed) {
			StartCoroutine ("DestroyAll");
		}
	}

	IEnumerator DestroyAll() {
		isDestroyed = true;
		for (int i = 0; i < sections.Length; i++) {
			sections [i].DestoryAll ();
			yield return new WaitForSeconds (0.2f);
		}
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Cells");
		for (int i = 0; i < go.Length ; i++) {
			go [i].GetComponent<CellListnerMeter> ().Fall();
		}

		yield return new WaitForSeconds (6);
		fader.FadeOut ();
		yield return new WaitForSeconds (2);
		SceneManager.LoadSceneAsync ("Damage");
	}
}
