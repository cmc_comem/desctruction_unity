﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionMeter : MonoBehaviour {

	public bool isWindow = false;
	public GameObject newObject;
	public GameObject brokenObject;
	public float destructionState;
	public float hitMeter = 0f;
	public float maxHitMeter = 4f;
	public float collapseRatio = 0.6f;

	private CellListnerMeter[] cells;
	private MeterVar mv;
	private int totalCount;
	private int currentCount;

	// Use this for initialization
	void Start () {
		if (isWindow) {
			collapseRatio = 0.01f;
		}
		maxHitMeter = MeterVar._facadeStrength;
		newObject = transform.GetChild (0).gameObject;
		brokenObject = transform.GetChild (1).gameObject;
		brokenObject.SetActive (false);
		newObject.SetActive (true);
		mv = transform.GetComponentInParent<MeterVar> ();
		destructionState = 0;
		cells = brokenObject.GetComponentsInChildren<CellListnerMeter> ();
	}

	public void Hit(float impulse) {
		hitMeter += impulse;
		if (hitMeter > maxHitMeter) {
			brokenObject.SetActive (true);
			newObject.SetActive (false);
			totalCount = currentCount = cells.Length;
		}
	}

	public void CellDestroyed() {
		currentCount -= 1;
		destructionState = ((float)totalCount / currentCount) - 1;
		if (destructionState > collapseRatio) {
			foreach (var cell in cells) {
				cell.Fall ();
			}
			mv.SectionDestoryed ();
		}
	}

	public void DestoryAll() {
		newObject.SetActive (false);
		brokenObject.SetActive (true);
		foreach (var cell in cells) {
			if (cell != null) {
				cell.Fall ();
			}
		}
	}
}
