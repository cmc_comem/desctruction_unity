﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildColliderMeter : MonoBehaviour {

	void OnCollisionEnter(Collision coll) {
		if (coll.collider.name == "Bullet(Clone)" || coll.collider.tag == "Cell") {
			transform.parent.GetComponent<DestructionMeter> ().Hit(coll.impulse.magnitude);
		}
	}
}
