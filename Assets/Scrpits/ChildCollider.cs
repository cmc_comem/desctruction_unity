﻿using UnityEngine;
using System.Collections;

public class ChildCollider : MonoBehaviour {

	void OnCollisionEnter(Collision coll) {
		if (coll.collider.name == "Bullet(Clone)" || coll.collider.tag == "Cell") {
			transform.parent.GetComponent<Destruction> ().Hit();
		}
	}
}
