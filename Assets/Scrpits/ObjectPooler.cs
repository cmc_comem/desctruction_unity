﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {

	public GameObject pooledObject;
	public int pooledAmount = 20;
	public bool willGrow = true;
	public GameObject poolLocation;

	List<GameObject> pooledObjects;

	void Start () {
		if (poolLocation == null) {
			poolLocation = GameObject.Find ("Pool");
			if (poolLocation == null) {
				poolLocation = new GameObject ("Pool");
			}
		}

		pooledObjects = new List<GameObject> ();
		for (int i = 0; i < pooledAmount; i++) {
			GameObject obj = Instantiate (pooledObject);
			obj.transform.parent = poolLocation.transform;
			obj.SetActive (false);
			pooledObjects.Add (obj);

		}
	}

	public GameObject GetPooledObject() {
		for (int i = 0; i < pooledObjects.Count; i++){
			if(!pooledObjects[i].activeInHierarchy) {
				return pooledObjects[i];
			}
		}

		if(willGrow) {
			GameObject obj = Instantiate(pooledObject);
			obj.transform.SetParent (poolLocation.transform);
			pooledObjects.Add(obj);
			return obj;
		}

		return null;
	} 

}
