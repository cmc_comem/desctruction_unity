﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellListnerMeter : MonoBehaviour {

	private Rigidbody rb;
	private DestructionMeter parentDestruction;
	private float maxSize;

	public float hitMeter = 0f;
	public float maxHitMeter = 4f;

	void Start () {
		maxHitMeter = MeterVar._cellStrength;
		rb = GetComponent<Rigidbody> ();
		rb.isKinematic = true;
		parentDestruction = transform.parent.parent.GetComponent<DestructionMeter> ();
		maxSize = Mathf.Max (transform.lossyScale.y, transform.lossyScale.x, transform.lossyScale.z);
	}

	void OnCollisionEnter(Collision coll) {
		if (coll.gameObject.name == "OutOfBound") {
			Destroy (gameObject);
		} else {
			hitMeter += coll.impulse.magnitude;
			if (hitMeter >= maxHitMeter && rb.isKinematic) {
				rb.isKinematic = false;
				parentDestruction.CellDestroyed ();
			}
		}
	}

	public void Fall() {
		if (rb != null) {
			rb.isKinematic = false;
		}
	}
}
