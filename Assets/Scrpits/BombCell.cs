﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCell : MonoBehaviour {

	Vector3 localPos;
	Quaternion localRot;

	void OnEnable () {
		localPos = transform.localPosition;
		localRot = transform.localRotation;
		GetComponent<Rigidbody> ().isKinematic = false;
	}

	void OnDisable() {
		transform.localPosition = localPos;
		transform.localRotation = localRot;
		GetComponent<Rigidbody> ().isKinematic = true;
	}
}
